# Ejercicios del curso Introducción a la programación 


## Datos

Muñoz Nieves Ernesto 


## Ejercicios

- [X] [Ejercicio-01_Numeros_Pares](https://gitlab.com/ernt/ejercicios-programacion/-/tree/main/Ejercicio-01_Numeros_pares)
- [X] [Ejercicio-02_suma_consecutiva](https://gitlab.com/ernt/ejercicios-programacion/-/tree/main/Ejercicio-02_suma_consecutivos)
- [X] [Ejercicio-03_dolares_pesos](https://gitlab.com/ernt/ejercicios-programacion/-/tree/main/Ejercicio-03_dolares_pesos)
- [X] [Ejercicio-04_equilatero](https://gitlab.com/ernt/ejercicios-programacion/-/tree/main/Ejercicio-04_equilatero)
- [X] [Ejecicio-05_isosceles](https://gitlab.com/ernt/ejercicios-programacion/-/tree/main/Ejercicio-05_isosceles)
- [X] [Ejecicio-06_suma_escaleno](https://gitlab.com/ernt/ejercicios-programacion/-/tree/main/Ejercicio-06_suma_escaleno)
- [ ] [Ejecicio-07_PerimetroDelTriangulo](https://gitlab.com/ernt/ejercicios-programacion/-/tree/main/Ejercicio-07_PerimetroDelTriangulo)
- [ ] [Ejecicio-08_TablasDeMultiplicar](https://gitlab.com/ernt/ejercicios-programacion/-/tree/main/Ejercicio-08_TablasDeMultiplicar)
